// exception.cc 
//	Entry point into the Nachos kernel from user programs.
//	There are two kinds of things that can cause control to
//	transfer back to here from user code:
//
//	syscall -- The user code explicitly requests to call a procedure
//	in the Nachos kernel.  Right now, the only function we support is
//	"Halt".
//
//	exceptions -- The user code does something that the CPU can't handle.
//	For instance, accessing memory that doesn't exist, arithmetic errors,
//	etc.  
//
//	Interrupts (which can also cause control to transfer from user
//	code into the Nachos kernel) are handled elsewhere.
//
// For now, this only handles the Halt() system call.
// Everything else core dumps.
//
// Copyright (c) 1992-1996 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "main.h"
#include "syscall.h"
#include <string.h>
//----------------------------------------------------------------------
// ExceptionHandler
// 	Entry point into the Nachos kernel.  Called when a user program
//	is executing, and either does a syscall, or generates an addressing
//	or arithmetic exception.
//
// 	For system calls, the following is the calling convention:
//
// 	system call code -- r2
//		arg1 -- r4
//		arg2 -- r5
//		arg3 -- r6
//		arg4 -- r7
//
//	The result of the system call, if any, must be put back into r2. 
//
// And don't forget to increment the pc before returning. (Or else you'll
// loop making the same system call forever!
//
//	"which" is the kind of exception.  The list of possible exceptions 
//	are in machine.h.
//----------------------------------------------------------------------

void
ExceptionHandler(ExceptionType which)
{
	int	type = kernel->machine->ReadRegister(2);
	int	val;

    switch (which) {
	case SyscallException:
	    switch(type) {
		case SC_Halt:
		    DEBUG(dbgAddr, "Shutdown, initiated by user program.\n");
   		    kernel->interrupt->Halt();
		    break;
		case SC_PrintInt:
			val=kernel->machine->ReadRegister(4);
			cout << "Print integer:" <<val << endl;
			return;
		case SC_Sleep:
			val = kernel->machine->ReadRegister(4);// load register to variable
			cout << "Sleep sec:" << val << endl;  // print sleep string to terminal
			kernel->alarm->WaitUntil(val);       // call alarm to count time
			return;
		case SC_ThreadYield:
			cout << "call threadYield." << endl;
			kernel->currentThread->Yield();
			return;
		
		case SC_Log:
		{
			OpenFile *file;
			char myletterOffset =35%26;
			char myletter[2];
				myletter[0] ='a'+myletterOffset;
				myletter[1] ='A'+myletterOffset;
			char param[2];
			char ID[20]=		"[B10915035_Log]";
			char fileName[20]=	"NachOS.log";
			char ERROR[10]=		"error";
			char newline[2] =	"\n";
			char MSG[80]={0};

			param[0] = kernel-> machine ->ReadRegister(4);
			file     = kernel-> fileSystem ->Open(fileName);
			cout << param << endl;
			int len = file->Length();			
			strcat(MSG,ID);

			if(param[0]==myletter[0]||
			   param[0]==myletter[1])
			{
				strcat(MSG,ERROR);//log error				
			}else{
				strcat(MSG,param);//log with param
			}
			strcat(MSG,newline);
			file->WriteAt(MSG,strlen(MSG),len);
			
			return;
		}
/*		case SC_Exec:
			DEBUG(dbgAddr, "Exec\n");
			val = kernel->machine->ReadRegister(4);
			kernel->StringCopy(tmpStr, retVal, 1024);
			cout << "Exec: " << val << endl;
			val = kernel->Exec(val);
			kernel->machine->WriteRegister(2, val);
			return;
*/		case SC_Exit:
			DEBUG(dbgAddr, "Program exit\n");
			val=kernel->machine->ReadRegister(4);
			cout << "return value:" << val << endl;
			kernel->currentThread->Finish();
			break;
		default:
		    cerr << "Unexpected system call " << type << "\n";
 		    break;
	    }
	    break;
	case PageFaultException:
		/*    Page Fault Exception    */
	    break;
	default:
	    cerr << "Unexpected user mode exception" << which << "\n";
	    break;
    }
    ASSERTNOTREACHED();
}
